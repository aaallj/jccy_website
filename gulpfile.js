var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    less = require('gulp-less');


gulp.task('default', function(){
  console.log('gulp run successfully!');
});

gulp.task('css_zh', function(){
  return gulp.src(['./public/stylesheets/less/zh/*.less', '!./public/stylesheets/less/zh/*.import.less'])
        .pipe(less())
        .pipe(autoprefixer())
        .pipe(minifycss())
        .pipe(gulp.dest('./public/stylesheets/zh'));
});

gulp.task('css_en', function(){
  return gulp.src(['./public/stylesheets/less/en/*.less', '!./public/stylesheets/less/en/*.import.less'])
        .pipe(less())
        .pipe(autoprefixer())
        .pipe(minifycss())
        .pipe(gulp.dest('./public/stylesheets/en'));
});

gulp.task('watch_zh',function(){
  gulp.watch('./public/stylesheets/less/zh/*.less', ['css_zh']);
});

gulp.task('watch_en',function(){
  gulp.watch('./public/stylesheets/less/en/*.less', ['css_en']);
});

gulp.task('watch', function(){
  gulp.watch('./public/stylesheets/less/*/*.less', ['css_zh','css_en']);
});
