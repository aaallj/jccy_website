var jccy = window.jccy || {};
jccy.locationSearch = {};

/*scrollPicture*/
jccy.scrollPicture = {
  main : function(obj,opt){
    var next = opt.next,
        prev = opt.prev,
        page = opt.page,
        nav = opt.nav,
        item = opt.item,
        showMethod = opt.showMethod || 'move',
        ulWidth = opt.width,
        cur = 0,left,
        timer = null,
        clickTimer = null,
        move;
        
    var move = function(){
      if(showMethod=='move'){
        left = -cur * ulWidth;
        obj.animate({left:left+'px'},'slow');
      } else if(showMethod=="opacity"){
        item.removeClass('select');
        item.eq(cur).addClass('select');
      }
      nav.find('a').removeClass('select');
      nav.find('a').eq(cur).addClass('select');
      
    };
    if(showMethod=='move'){
      move = function(){
        left = -cur * ulWidth;
        obj.animate({left:left+'px'},'slow');
        nav.find('a').removeClass('select');
        nav.find('a').eq(cur).addClass('select');
      };
    } else if(showMethod=="opacity"){
      move = function(){
        item.removeClass('select');
        item.eq(cur).addClass('select');
        nav.find('a').removeClass('select');
        nav.find('a').eq(cur).addClass('select');
      };
    }

    var autoPlay=function(){
      clearInterval(timer);
      clearInterval(clickTimer);
      timer=setInterval(function(){
        cur++;
        if(cur>=page){
          cur = 0;
        }
        move();
      },3500);
    };
    autoPlay();
    obj.hover(function(){
      clearInterval(timer);
      clearInterval(clickTimer);
    },function(){
      clickTimer=setTimeout(function(){
        autoPlay();
      },2000);
    });
    nav.delegate('a','click',function(){
      var index = $(this).index();
      cur = index;
      clearInterval(timer);
      clearInterval(clickTimer);
      move();
      clickTimer=setTimeout(function(){
        autoPlay();
      },2000);
    });
    if(prev){
      prev.click(function(){
        cur--;
        if(cur<0){
          cur = page-1;
        }
        clearInterval(timer);
        clearInterval(clickTimer);
        move();
        clickTimer=setTimeout(function(){
          autoPlay();
        },2000);
      });
    }
    if(next){
      next.click(function(){
        cur++;
        if(cur>=page){
          cur = 0;
        }
        clearInterval(timer);
        clearInterval(clickTimer);
        move();
        clickTimer=setTimeout(function(){
          autoPlay();
        },2000);
      });
    }
    
  },
  init : function(obj,opt){
    this.main(obj,opt);
  }
};  

var WhichTheme = function(){

};

WhichTheme.prototype.init = function(){

  var search = window.location.search.substring(1),
      arr = search.split('&'), arr1;
  
  for(var i=0,len=arr.length;i<len;i++){
      arr1 = arr[i].split('=');
      jccy.locationSearch[arr1[0]] = arr1[1];
  }
  this.showTheme(jccy.locationSearch['theme_id']);

};

WhichTheme.prototype.showTheme = function(id){
  var obj = $('.reveal-wrap'),
      pic = obj.find('.reveal-pic'), 
      text = obj.find('.reveal-text'),
      html = [];
  for(var i=0;i<3;i++){
    html.push('<li>');
    html.push('<img src="/themes/'+ id +'/theme_preview_'+(i+1)+'.jpg" alt="">');
    html.push('</li>');
  }
  pic.find('ul').html( html.join('') );
  obj.find('.download-btn a').attr('href', '/themes/'+ id +'/me.chengen.launcher.theme.apk');
  text.find('h3').html();
};




