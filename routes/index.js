var express = require('express');
var acceptLanguage = require('accept-language');
var country = 'en';

var router = express.Router();

var themeDesc = [{
    "title": "Clash of kings",
    "describe": "Clash of kings theme, enjoy fantastic battle game experience.",
    "link": "https://play.google.com/store/apps/details?id=com.abclauncher.theme.clash_of_kings"
  },{
    "title": "Rose red",
    "describe": "Fancy rose red graphic patterns to cheer you up! ",
    "link": "https://play.google.com/store/apps/details?id=com.abclauncher.theme.rose_red"
  },{
    "title": "A city of romance",
    "describe": "City full of romance is waiting for you to explore.",
    "link": "https://play.google.com/store/apps/details?id=com.abclauncher.theme.a_city_of_romance"
  },{
    "title": "Fresh girl",
    "describe": "Let's have fun in fresh girl theme.",
    "link": "https://play.google.com/store/apps/details?id=com.abclauncher.theme.Fresh_girl"
  },{
    "title": "Black box",
    "describe": "Let's see what happens in black box.",
    "link": "https://play.google.com/store/apps/details?id=com.abclauncher.theme.black_box"
  },{
    "title": "Forest trees",
    "describe": "Forest trees theme make you remember those good old days.",
    "link": "https://play.google.com/store/apps/details?id=com.abclauncher.theme.forest_trees"
  },{
    "title": "Simple style",
    "describe": "Complexity needs to be dealt with a simple style.",
    "link": "https://play.google.com/store/apps/details?id=com.abclauncher.theme.simple_style"
  },{
    "title": "Transparent glass",
    "describe": "Hello from the other side of transparent glass.",
    "link": "https://play.google.com/store/apps/details?id=com.abclauncher.theme.transparent_glass"
  }];;


/*function getClientIp(req) {
  return req.headers['x-forwarded-for'] ||
    req.connection.remoteAddress ||
    req.socket.remoteAddress ||
    req.connection.socket.remoteAddress;
};

function getCountryCode(ip) {
  var geo = geoip.lookup(ip);
  var country = (geo && geo.country) ? geo.country.toLowerCase() : "cn";
  return country;
}; */

/*router.use(function getCountry(req, res, next){
var languageStr, languageObj, language;

  if(!req.cookies.language){

    languageStr = req.headers["accept-language"];
    languageObj = acceptLanguage.parse(languageStr);
    if(languageObj[0]){
      country = languageObj[0]['language'];
    }

    if(country !== 'zh'){
      country = "en";
    }
    res.cookie('language', country, {
      path: '/',
      maxAge: 1000*60*60*24*365
    });
  } else{
    country = req.cookies.language;
  }

  next();

});*/


/* GET home page. */
router.get('/', function(req, res) {

  res.render(country+'/index', { data: themeDesc });
});
router.get('/themes-list/', function(req, res){
  res.render(country+'/themes-list', { data: themeDesc })
});
router.get('/theme/:themeid', function(req, res){
  var themeid = req.params.themeid,
      num = themeid.slice(-1);

  res.render(country+'/theme', { data: themeDesc, curData: themeDesc[num], number: num })
});
router.get('/about-us/', function(req, res){
  res.render(country+'/about-us', { title: 'Express' })
});
router.get('/logs/', function(req, res){
  res.render(country+'/logs', { title: 'Express' })
});
router.get('/terms-of-service/', function(req, res){
  res.render(country+'/terms-of-service', { title: 'Express' })
});
router.get('/help/', function(req, res){
  res.render(country+'/help', { title: 'Express' })
});
router.get('/about-abc/', function(req, res){
  res.render(country+'/about-abc', { title: 'Express' })
});
router.get('/licence/', function(req, res){
  res.render(country+'/licence', { title: 'Express' })
});
router.get('/privacy/', function(req, res){
  res.render(country+'/privacy', { title: 'Express' })
});
router.get('/contact-us/', function(req, res){
  res.render(country+'/contact-us', { title: 'Express' })
});




module.exports = router;
